# AutoTestRunner
- Tests getter/setter conformity and validity.
- Tests hashCode and equals conformity and validity.
- Tests toString.

## Example
A class and it's test:
```java
public final class Pet {
    private Integer age;
    private String name;

    public Pet() {
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(final Integer age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Pet pet = (Pet) o;

        if (age != null ? !age.equals(pet.age) : pet.age != null) return false;
        return name != null ? name.equals(pet.name) : pet.name == null;

    }

    @Override
    public int hashCode() {
        int result = age != null ? age.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }
}
```
```java
@RunWith(AutoTestRunner.class)
public class PetTest {
    @Test
    public void runTests() {
        // nothing
    }
}
```