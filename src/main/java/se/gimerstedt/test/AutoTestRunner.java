package se.gimerstedt.test;

import com.openpojo.validation.ValidatorBuilder;
import com.openpojo.validation.rule.impl.GetterMustExistRule;
import com.openpojo.validation.rule.impl.NoPublicFieldsExceptStaticFinalRule;
import com.openpojo.validation.rule.impl.SetterMustExistRule;
import com.openpojo.validation.test.impl.GetterTester;
import com.openpojo.validation.test.impl.SetterTester;
import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import org.mockito.runners.MockitoJUnitRunner;

import java.lang.reflect.InvocationTargetException;

import static com.openpojo.reflection.impl.PojoClassFactory.getPojoClass;
import static java.lang.Class.forName;
import static org.junit.Assert.assertTrue;

/**
 * Automatically tests getter/setter/equals validity on pojos,
 * check out {@link AutoTestRunnerConfig} for configuration details
 */
public class AutoTestRunner extends MockitoJUnitRunner {
    private Class<?> classToTest;
    private AutoTestRunnerConfig config;

    public AutoTestRunner(Class<?> clazz)
            throws InvocationTargetException, ClassNotFoundException, InstantiationException,
            IllegalAccessException, NoSuchMethodException, SecurityException {
        super(clazz);
        classToTest = getClassToTest(clazz);
        config = clazz.getAnnotation(AutoTestRunnerConfig.class);
        try {
            verifyEquals();
        } catch (NoSuchMethodException nsme) {
        }
        verifyGettersAndSetters();
        verifyToString();
    }

    private Class<?> getClassToTest(Class<?> clazz) throws ClassNotFoundException {
        return forName(clazz.getName().substring(0, clazz.getName().length() - 4));
    }

    private void verifyGettersAndSetters() {
        if (config == null || config.getters() || config.setters()) {
            ValidatorBuilder validatorBuilder = ValidatorBuilder.create();
            validatorBuilder.with(new NoPublicFieldsExceptStaticFinalRule());
            if (config == null || config.getters()) {
                validatorBuilder.with(new GetterMustExistRule());
                validatorBuilder.with(new GetterTester());
            }
            if (config == null || config.setters()) {
                validatorBuilder.with(new SetterMustExistRule());
                validatorBuilder.with(new SetterTester());
            }
            validatorBuilder.build().validate(getPojoClass(classToTest));
        }
    }

    private void verifyEquals() throws NoSuchMethodException {
        classToTest.getDeclaredMethod("equals", Object.class);
        EqualsVerifier<?> equalsVerifier = EqualsVerifier.forClass(classToTest);

        if (config != null) {
            String[] exclude = config.equalsExclude();
            String[] include = config.equalsInclude();

            if (exclude.length > 0 && include.length > 0) {
                throw new IllegalArgumentException("Include and Exclude are mutually exclusive.");
            } else if (exclude.length > 0) {
                equalsVerifier.withIgnoredFields(exclude);
            } else if (include.length > 0) {
                equalsVerifier.withOnlyTheseFields(include);
            }
        }
        equalsVerifier.suppress(Warning.STRICT_INHERITANCE, Warning.NONFINAL_FIELDS, Warning.INHERITED_DIRECTLY_FROM_OBJECT).verify();
    }

    private void verifyToString() throws InstantiationException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {
        assertTrue(classToTest.getConstructor().newInstance().toString()
                .contains(classToTest.getSimpleName()));
    }
}
