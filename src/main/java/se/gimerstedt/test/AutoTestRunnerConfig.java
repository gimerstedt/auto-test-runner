package se.gimerstedt.test;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation for configuring {@link AutoTestRunner}
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface AutoTestRunnerConfig {
    /**
     * Which fields to exclude from equals verification.
     * Mutually exclusive with equalsInclude.
     */
    String[] equalsExclude() default {};

    /**
     * Which fields to include for equals verification.
     * Mutually exclusive with equalsExclude.
     */
    String[] equalsInclude() default {};

    /**
     * If getters are to be verified.
     */
    boolean getters() default true;

    /**
     * if setters are to be verified.
     */
    boolean setters() default true;
}
