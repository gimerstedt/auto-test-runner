package se.gimerstedt.test.testpojos;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
public class PojoWithBothIncludeAndExcludeForEquals {
    private String name;
}
