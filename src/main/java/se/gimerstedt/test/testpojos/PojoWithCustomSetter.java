package se.gimerstedt.test.testpojos;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@EqualsAndHashCode
public class PojoWithCustomSetter {
    private String name;

    public void setName(String name) {
        this.name = "patrik";
    }
}
