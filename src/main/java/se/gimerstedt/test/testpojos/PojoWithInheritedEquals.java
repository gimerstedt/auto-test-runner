package se.gimerstedt.test.testpojos;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class PojoWithInheritedEquals {
    private String name;
}
