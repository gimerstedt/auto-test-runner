package se.gimerstedt.test.testpojos;

import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@NoArgsConstructor
@EqualsAndHashCode(doNotUseGetters = true)
public class PojoWithCustomGetter {
    private String name;

    public String getName() {
        return "patrik";
    }
}
