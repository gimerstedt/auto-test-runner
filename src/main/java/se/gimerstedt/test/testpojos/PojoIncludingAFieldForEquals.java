package se.gimerstedt.test.testpojos;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(of = {"name"})
public class PojoIncludingAFieldForEquals {
    private String name;
    private String notName;
}
