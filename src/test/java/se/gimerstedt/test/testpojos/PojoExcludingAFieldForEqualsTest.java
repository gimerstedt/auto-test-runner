package se.gimerstedt.test.testpojos;

import org.junit.Test;
import org.junit.runner.RunWith;
import se.gimerstedt.test.AutoTestRunner;
import se.gimerstedt.test.AutoTestRunnerConfig;

@RunWith(AutoTestRunner.class)
@AutoTestRunnerConfig(equalsExclude = {"ignoreForEquals"})
public class PojoExcludingAFieldForEqualsTest {
    @Test
    public void runTests() {
    }
}
