package se.gimerstedt.test.testpojos;

import org.junit.Test;
import se.gimerstedt.test.AutoTestRunnerConfig;

/**
 * Executed explicitly from {@link se.gimerstedt.test.AutoTestRunnerTest}.
 */
@AutoTestRunnerConfig(equalsInclude = {"name"}, equalsExclude = {"name"})
public class PojoWithBothIncludeAndExcludeForEqualsTest {
    @Test
    public void runTests() {
    }
}
