package se.gimerstedt.test.testpojos;

import org.junit.Test;
import org.junit.runner.RunWith;
import se.gimerstedt.test.AutoTestRunner;
import se.gimerstedt.test.AutoTestRunnerConfig;

import static junit.framework.TestCase.assertEquals;

@RunWith(AutoTestRunner.class)
@AutoTestRunnerConfig(getters = false)
public class PojoWithCustomGetterTest {
    @Test
    public void runTests() {
        PojoWithCustomGetter c = new PojoWithCustomGetter();
        c.setName("hej");
        assertEquals("patrik", c.getName());
    }
}
