package se.gimerstedt.test.testpojos;

import org.junit.Test;
import org.junit.runner.RunWith;
import se.gimerstedt.test.AutoTestRunner;
import se.gimerstedt.test.AutoTestRunnerConfig;

@RunWith(AutoTestRunner.class)
@AutoTestRunnerConfig(equalsInclude = {"name"})
public class PojoIncludingAFieldForEqualsTest {
    @Test
    public void runTests() {
    }
}
