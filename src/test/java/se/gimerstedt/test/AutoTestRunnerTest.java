package se.gimerstedt.test;

import org.junit.Test;
import se.gimerstedt.test.testpojos.PojoWithBothIncludeAndExcludeForEqualsTest;

import java.lang.reflect.InvocationTargetException;

public class AutoTestRunnerTest {
    @Test(expected = IllegalArgumentException.class)
    public void throwsExceptionWhenBothIncludeAndExcludeForEqualsIsSpecified() throws ClassNotFoundException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {
        new AutoTestRunner(PojoWithBothIncludeAndExcludeForEqualsTest.class);
    }
}
